(function() {
    'use strict';

    angular
        .module('adminsite.CusRequest')
        .factory('requestService', Service);
    
    Service.inject = ['$http'];
    function Service($http) {
        var service = {
            exposedFn: exposedFn,
            getListSkillsFn: getListSkillsFn,
            getListCusRequestsFn: getListCusRequestsFn
        };
        
        return service;

        ////////////////
        function getListSkillsFn() {
            return $http.get("/app/data/listSkills.json").then(
                function successCallback(response) {
                    return response.data;
                },
                function errorCallback(response) {
                    return response.error;
                }
            );
        }

        function getListCusRequestsFn(query) {
            return $http.get("/app/data/listCusRequest.json").then (
                function successCallback(response) {
                    var result = [];
                    result.total = response.data.length;
                    result.data = response.data.splice(query.limit * (query.page - 1), query.limit);
                    return result;
                },
                function errorCallback(response) {
                    return response.error;
                }
            );
        }

        function exposedFn() { }
    }
})();