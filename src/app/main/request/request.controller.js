(function() {
    'use strict';

    angular
        .module('adminsite.CusRequest')
        .controller('RequestController', ControllerController);

    ControllerController.inject = ['requestService', '$log'];
    function ControllerController(requestService, $log) {
        var vm = this;
        
        vm.query = {
            'limit': 5,
            'page': 1
        };

        activate();
        vm.getListCusReqFn = getListCusReqFn;

        ////////////////

        function activate() { 
            requestService.getListSkillsFn().then(function(response){
                vm.skills = response;
            });
            requestService.getListCusRequestsFn(vm.query).then(function(response){
                vm.cusReq = response;
                $log.info(response);
            });
        }

        function getListCusReqFn() {

            requestService.getListCusRequestsFn(vm.query).then(function(response){
                vm.cusReq = response;
                $log.info(response);
            });
        }
    }
})();