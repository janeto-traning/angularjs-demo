(function() {
    'use strict';

    angular
        .module('adminsite')
        .controller('IndexController', ControllerController);

    ControllerController.inject = ['$state'];
    function ControllerController($state) {
        var vm = this;


        vm.isHide = false;
        vm.menu = [{
            'name': 'Yêu cầu khách hàng',
            'state': 'requests'
        }, {
            'name': 'Quản lý nhân viên',
            'state': 'employees',
            'item': [{
                'name': 'Danh sách nhân viên',
                'state': 'listEmployee'
            }, {
                'name': 'Lương nhân viên',
                'state': 'salaryEmployee'
            }, {
                'name': 'Tài khoản nhân viên',
                'state': 'accountEmployee'
            }]
        }, {
            'name': 'Quản lý dịch vụ',
            'state': 'services'
        }, {
            'name': 'Quản lý khách hàng',
            'state': 'customers'
        }, {
            'name': 'Thống kê',
            'state': 'reports'
        }];

        vm.goState = goState;

        function goState (st) {
            if (st === "requests") {
                $state.go(st);
            }
        }

        activate();

        ////////////////

        function activate() { }
    }
})();